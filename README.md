

### Setup Go to get go modules private
```bash
echo """
[url \"ssh://git@gitlab.com/trkbit/\"]
	insteadOf = https://gitlab.com/trkbit/
""" >> ~/.gitconfig

export GOPRIVATE=gitlab.com/trkbit
```