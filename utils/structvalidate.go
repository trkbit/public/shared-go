package utils

import (
	"fmt"

	"github.com/go-playground/validator/v10"
)

// use a single instance of Validate, it caches struct info
var validate *validator.Validate

type invalidArgument struct {
	Field string `json:"field"`
	Value any    `json:"value"`
	Tag   string `json:"tag"`
	Param string `json:"param"`
}

type InvalidArgs []invalidArgument

func (s InvalidArgs) Error() string {
	serr := ""
	for _, v := range s {
		serr = serr + v.Field + " "
	}
	return fmt.Sprintf("invalid arguments on validate: %s", serr)
}

func init() {
	validate = validator.New(validator.WithRequiredStructEnabled())

}

func ValidateStruct(obj any) error {
	err := validate.Struct(obj)
	if errs, ok := err.(validator.ValidationErrors); ok {
		var invalidArgs InvalidArgs = []invalidArgument{}

		for _, err := range errs {
			invalidArgs = append(invalidArgs, invalidArgument{
				err.Field(),
				err.Value(),
				err.Tag(),
				err.Param(),
			})
		}

		return invalidArgs
	}

	return nil
}
