package utils

import (
	"encoding/json"
	"log"
	"testing"
)

func Test_ValidateStruct(t *testing.T) {
	var testeObj struct {
		Name string `json:"name" validate:"required"`
	} = struct {
		Name string "json:\"name\" validate:\"required\""
	}{}

	if err := ValidateStruct(testeObj); err != nil {
		b, _ := json.Marshal(err)
		log.Println(string(b))
		t.Error(err)
	}
}
