package trkbitsharedgo

import (
	"strings"

	"github.com/paemuri/brdoc"
)

func BrDocValidator(docnumber string) (string, bool) {
	docnumber = strings.ReplaceAll(docnumber, " ", "")

	switch {
	case brdoc.IsCNPJ(docnumber):
		return "CNPJ", true
	case brdoc.IsCPF(docnumber):
		return "CPF", true
	default:
		return "", false
	}
}
