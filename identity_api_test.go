package trkbitsharedgo

import (
	"log"
	"testing"
)

func Test_identity_api_GetCpf(t *testing.T) {
	api := NewApi("")

	result, err := api.GetCpf("44593127866", false)
	if err != nil {
		t.Errorf("api.GetCpf() error = %v", err)
	}

	tas, errAge := result.Has18Years()
	if errAge != nil {
		log.Println(errAge)
	}

	log.Println(tas)
}
