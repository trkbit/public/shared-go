package trkbitsharedgo

import (
	"fmt"
	"io"
	"net/http"
)

func makeRequest(method string, uri string, headers map[string]string, body io.Reader) (*http.Request, error) {
	request, err := http.NewRequest(method, uri, body)
	if err != nil {
		return nil, err
	}

	for k, v := range headers {
		request.Header.Add(k, v)
	}

	return request, nil
}

type request struct {
	client *http.Client
}

func (s request) Do(method string, uri string, headers map[string]string, body io.Reader) ([]byte, error) {
	req, err := makeRequest(method, uri, headers, body)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	payload, _ := io.ReadAll(resp.Body)

	statusOK := resp.StatusCode >= 200 && resp.StatusCode < 300
	if !statusOK {
		return payload, fmt.Errorf("http response status code %d", resp.StatusCode)
	}

	return payload, nil
}
