package types

import "encoding/json"

type ErrorResp struct {
	Code  int         `json:"code"`
	Error interface{} `json:"error"`
}

type Error struct {
	Code  int         `json:"code"`
	Error interface{} `json:"error"`
}

type DataResp struct {
	Data any `json:"data"`
}

type RawData []byte

func (s RawData) JsonUnmarshal(d interface{}) error {
	return json.Unmarshal(s, d)
}
