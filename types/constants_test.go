package types

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"testing"
)

func Test_constants(t *testing.T) {

	var a EnumEventOrigin = EventOriginBoleto

	log.Println(a)
}

func Test_CurrencySystemString(t *testing.T) {

	var a CurrencySystem = CurrencySystem(8)

	log.Println(a)
}

func Test_CurrencyIntPair(t *testing.T) {
	var a AllowedCurrencyPair = "1:6"
	log.Println(a.CurrencyTypePair())
}

func Test_BankNumberCurrencySystem(t *testing.T) {
	log.Println(int(BankNumberCurrencySystem("004")))
}

func Test_BankToCurrencySyste(t *testing.T) {
	var bankscode []BrazilianBankBcb = make([]BrazilianBankBcb, 0)
	var output []string = make([]string, 0)

	json.Unmarshal(BRAZILIAN_BANKS_CODE, &bankscode)

	bankcount := 8
	for _, b := range bankscode {
		if b.CodeNumber == "001" || fmt.Sprintf("%v", b.CodeNumber) == "323" || b.CodeNumber == "082" || fmt.Sprintf("%v", b.CodeNumber) == "208" {
			continue
		}
		bankcount++
		output = append(output, `"`+b.ShortName.(string)+`"`)
	}
	log.Println(strings.Join(output, ","))
}

func Test_BankToCurrencySystemMap(t *testing.T) {
	var bankscode []BrazilianBankBcb = make([]BrazilianBankBcb, 0)
	var output map[string]int = make(map[string]int)

	json.Unmarshal(BRAZILIAN_BANKS_CODE, &bankscode)

	bankcount := 8
	for _, b := range bankscode {
		if b.CodeNumber == "001" || fmt.Sprintf("%v", b.CodeNumber) == "323" || b.CodeNumber == "082" || fmt.Sprintf("%v", b.CodeNumber) == "208" {
			continue
		}
		bankcount++
		output[fmt.Sprintf("%v", b.CodeNumber)] = bankcount

		if b.CodeNumber == "n/a" {
			fmt.Printf(`"%v":%d,`, b.ShortName, bankcount)
			continue
		}
		fmt.Printf(`"%v":%d,`, b.CodeNumber, bankcount)
	}
}
