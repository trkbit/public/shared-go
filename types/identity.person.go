package types

import (
	"fmt"
	"time"

	"github.com/google/uuid"
)

type PersonStatus int

func (s PersonStatus) StrToStatus(strstatus string) PersonStatus {
	switch strstatus {
	case "Regular":
		return PersonStatusRegular
	case "Irregular":
		return PersonStatusIrregular
	case "Cancelada":
		return PersonStatusCanceled
	case "Pendente":
		return PersonStatusPending
	case "Suspensa":
		return PersonStatusSuspended
	case "Nula":
		return PersonStatusNull
	default:
		return PersonNoStatus
	}
}

const (
	PersonNoStatus PersonStatus = iota
	PersonStatusIrregular
	PersonStatusRegular
	PersonStatusCanceled
	PersonStatusSuspended
	PersonStatusPending
	PersonStatusNull
)

type Person struct {
	Uid       string       `json:"uid" bson:"uid"`
	Document  string       `json:"document" bson:"document"`
	Status    PersonStatus `json:"status" bson:"status"`
	Name      string       `json:"name" bson:"name"`
	Birthdate string       `json:"birthdate" bson:"birthdate"`
	IsPep     bool         `json:"is_pep" bson:"is_pep"`
	Address   *Address     `json:"address,omitempty" bson:"address,omitempty"`
	Contact   `json:"contact,omitempty" bson:"contact,omitempty"`
}

func NewPerson(document string) *Person {
	return &Person{
		Uid:      uuid.NewString(),
		Status:   PersonNoStatus,
		Document: document,
		Address:  &Address{},
		Contact: Contact{
			Emails: make([]string, 0),
			Phones: make([]string, 0),
		},
	}
}

func (s *Person) ToUpdate() *PersonUpdate {
	return &PersonUpdate{
		Document:  s.Document,
		Status:    s.Status,
		Name:      s.Name,
		Birthdate: s.Birthdate,
		IsPep:     s.IsPep,
		Address:   s.Address,
	}
}

func (s Person) Has18Years() (bool, error) {
	if s.Birthdate == "" {
		return false, fmt.Errorf("no birthdate")
	}
	timeBirthdate, err := time.Parse(DATE_SIMPLE_FORMAT, s.Birthdate)
	if err != nil {
		return false, fmt.Errorf("birthdate parse error")
	}

	now := time.Now()

	diff := int64(now.Sub(timeBirthdate).Hours() / 24 / 365)
	if diff >= 18 {
		return true, nil
	}
	return false, fmt.Errorf("does not has 18 years")
}

func (s Person) Has16Years() (bool, error) {
	if s.Birthdate == "" {
		return false, fmt.Errorf("no birthdate")
	}
	timeBirthdate, err := time.Parse(DATE_SIMPLE_FORMAT, s.Birthdate)
	if err != nil {
		return false, fmt.Errorf("birthdate parse error")
	}

	now := time.Now()

	diff := int64(now.Sub(timeBirthdate).Hours() / 24 / 365)
	if diff >= 18 {
		return true, nil
	}
	return false, fmt.Errorf("does not has 16 years")
}

func (s Person) Has15Years() (bool, error) {
	if s.Birthdate == "" {
		return false, fmt.Errorf("no birthdate")
	}
	timeBirthdate, err := time.Parse(DATE_SIMPLE_FORMAT, s.Birthdate)
	if err != nil {
		return false, fmt.Errorf("birthdate parse error")
	}

	now := time.Now()

	diff := int64(now.Sub(timeBirthdate).Hours() / 24 / 365)
	if diff >= 15 {
		return true, nil
	}
	return false, fmt.Errorf("does not has 15 years")
}

func (s Person) IsRegular() bool {
	return s.Status == PersonStatusRegular
}

type PersonUpdate struct {
	Document  string       `json:"document,omitempty" bson:"document,omitempty"`
	Status    PersonStatus `json:"status,omitempty" bson:"status,omitempty"`
	Name      string       `json:"name,omitempty" bson:"name,omitempty"`
	Birthdate string       `json:"birthdate,omitempty" bson:"birthdate,omitempty"`
	IsPep     bool         `json:"is_pep,omitempty" bson:"is_pep,omitempty"`
	Address   *Address     `json:"address,omitempty" bson:"address,omitempty"`
}
