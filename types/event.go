package types

import (
	"encoding/json"

	"gitlab.com/trkbit/public/shared-go/utils"
)

type Event struct {
	Name string `json:"name"`
	Data any    `json:"data"`
}

func (s Event) JSONMarshal() ([]byte, error) {
	return json.Marshal(s)
}

type PaymentEventData struct {
	CreatedAt                  int64           `json:"created_at" bson:"created_at" validate:"required"`
	Type                       EventType       `json:"type" bson:"type"`
	Origin                     EnumEventOrigin `json:"origin" bson:"origin"`
	ChargeId                   string          `json:"charge_id" bson:"charge_id"`
	IdentityId                 string          `json:"identity_id" bson:"identity_id"`
	IdentityNickname           string          `json:"identity_nickname" bson:"identity_nickname"`
	CurrencySystem             any             `json:"currency_system" bson:"currency_system" validate:"required"`
	Currency                   Currency        `json:"currency" bson:"currency"`
	Amount                     float64         `json:"amount" bson:"amount" validate:"required"`
	OriginId                   string          `json:"origin_id" bson:"origin_id" validate:"required"`
	ReceiverCurrencySystem     CurrencySystem  `json:"recv_currency_system" bson:"recv_currency_system"`
	ReceiverCurrencySystemBody string          `json:"recv_currency_system_body" bson:"recv_currency_system_body"`
}

func (s PaymentEventData) Validate() error {
	return utils.ValidateStruct(s)
}

type IdentityEventData struct {
	Document string `json:"document" validate:"required"`
}

func (s IdentityEventData) Validate() error {
	return utils.ValidateStruct(s)
}
