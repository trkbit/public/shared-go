package types

import (
	"github.com/google/uuid"
)

type CompanyStatus int

const (
	CompanyNoStatus CompanyStatus = iota
	CompanyStatusInactive
	CompanyStatusActive
)

type Company struct {
	Uid          string        `json:"uid" bson:"uid"`
	Document     string        `json:"document" bson:"document"`
	Name         string        `json:"name" bson:"name"`
	TradingName  string        `json:"trading_name" bson:"trading_name"`
	ShareCapital string        `json:"share_capital" bson:"share_capital"`
	StartDate    string        `json:"start_date" bson:"start_date"`
	Status       CompanyStatus `json:"status" bson:"status"`
	Address      *Address      `json:"address,omitempty" bson:"address,omitempty"`
	Partners     []Partner     `json:"partners" bson:"partners"`
	Contact      `json:"contact,omitempty" bson:"contact,omitempty"`
}

func NewCompany(document string) *Company {
	return &Company{
		Uid:      uuid.NewString(),
		Document: document,
		Address:  &Address{},
		Partners: make([]Partner, 0),
	}
}

func (s Company) ToUpdate() *CompanyUpdate {
	return &CompanyUpdate{
		Document: s.Document,
	}
}

func (s Company) IsActive() bool {
	return s.Status == CompanyStatusActive
}

type CompanyUpdate struct {
	Document string `json:"document,omitempty" bson:"document,omitempty"`
}

type Partner struct {
	Name           string     `json:"name" bson:"name"`
	HiddenDocument string     `json:"hidden_document" bson:"hidden_document"`
	PersonType     PersonType `json:"person_type" bson:"person_type"`
	StartDate      string     `json:"start_date" bson:"start_date"`
}
