package types

import (
	"fmt"
	"slices"
	"strconv"
	"strings"
)

type PersonType int

const (
	DATE_SIMPLE_FORMAT     = "20060102"
	DATETIME_SIMPLE_FORMAT = "20060602HH:mm:ss"
)

const (
	PersonTypePF PersonType = iota
	PersonTypePJ
)

type OriginEventStatus int

const (
	OriginEventStatusNone OriginEventStatus = iota
	OriginEventStatusPeding
	OriginEventStatusDeleted
	OriginEventStatusDone
)

func (s OriginEventStatus) String() string {
	return []string{"none", "pending", "deleted", "done"}[s]
}

type PaymentStatus int

const (
	PaymentNone PaymentStatus = iota // it just a payment created but not processed
	PaymentCanceled
	PaymentWaiting // it will be proccess or can be reproccess
	PaymentDoing   // it is processing status
	PaymentDone    // it finished
	PaymentFailure
	PaymentCheckingCompliance     // during payment proccess flow the payment needs a compliance
	PaymentComplianceSuccessDoing // after compliance back to proccess
	PaymentWaitingReceipt         // after proccess payment the receipt starts to proccess and payment wait for receipt finish
	PaymentFailureCompliance      // payment got error in compliance steps
)

func (s PaymentStatus) String() string {
	return []string{"none", "canceled", "waiting", "doing", "done", "failure", "checking compliance", "doing after compliance", "waiting receipt"}[s]
}

type CurrencySystem int

const (
	CurrencySystemUnknow CurrencySystem = iota
	CurrencySystemBitcoin
	CurrencySystemEthereum
	CurrencySystemPolygon
	CurrencySystemBrazzilianBank
	CurrencySystemBTGBank
	CurrencySystemTopazioBank
	CurrencySystemMercadoPago
	CurrencySystemTRON
	CurrencySystemBRBBCODEBRASILIASA
	CurrencySystemSelic
	CurrencySystemBacen
	CurrencySystemSANTINVESTSACFI
	CurrencySystemCCRSEARA
	CurrencySystemAGKCCSA
	CurrencySystemCONFNACCOOPCENTRAISUNICRED
	CurrencySystemÍNDIGOINVESTIMENTOSDTVMLTDA
	CurrencySystemCAIXAECONOMICAFEDERAL
	CurrencySystemSTN
	CurrencySystemBANCOINTER
	CurrencySystemCOLUNASADTVM
	CurrencySystemBCORIBEIRAOPRETOSA
	CurrencySystemBANCOBARISA
	CurrencySystemBCOCETELEMSA
	CurrencySystemEWALLYIPSA
	CurrencySystemBANCOSEMEAR
	CurrencySystemPLANNERCVSA
	CurrencySystemFDOGARANTIDORCRÉDITOS
	CurrencySystemBCOB3SA
	CurrencySystemBCORABOBANKINTLBRASILSA
	CurrencySystemCIELOIPSA
	CurrencySystemCCRDEABELARDOLUZ
	CurrencySystemBCOCOOPERATIVOSICREDISA
	CurrencySystemCREHNORLARANJEIRAS
	CurrencySystemBCOBNPPARIBASBRASILSA
	CurrencySystemCECMCOOPERFORTE
	CurrencySystemKIRTONBANK
	CurrencySystemBCOBRASILEIRODECRÉDITOSA
	CurrencySystemBCOBVSA
	CurrencySystemBANCOSICOOBSA
	CurrencySystemTRINUSCAPITALDTVM
	CurrencySystemBCOKEBHANADOBRASILSA
	CurrencySystemXPINVESTIMENTOSCCTVMSA
	CurrencySystemSISPRIMEDOBRASILCOOP
	CurrencySystemCMCAPITALMARKETSCCTVMLTDA
	CurrencySystemBCOMORGANSTANLEYSA
	CurrencySystemUBSBRASILCCTVMSA
	CurrencySystemTREVISOCCSA
	CurrencySystemCIPSASiloc
	CurrencySystemHIPERCARDBMSA
	CurrencySystemBCOJSAFRASA
	CurrencySystemUNIPRIMECOOPCENTRALLTDA
	CurrencySystemBCOTOYOTADOBRASILSA
	CurrencySystemPARATICFISA
	CurrencySystemBCOALFASA
	CurrencySystemBCOABNAMROSA
	CurrencySystemBCOCARGILLSA
	CurrencySystemTERRAINVESTIMENTOSDTVM
	CurrencySystemCECMDOSTRABPORTDAGVITOR
	CurrencySystemSOCINALSACFI
	CurrencySystemSERVICOOP
	CurrencySystemOZCORRETORADECÂMBIOSA
	CurrencySystemBANCOBRADESCARD
	CurrencySystemNOVAFUTURACTVMLTDA
	CurrencySystemFIDUCIASCMEPPLTDA
	CurrencySystemGOLDMANSACHSDOBRASILBMSA
	CurrencySystemCAMARAINTERBANCARIADEPAGAMENTOSCIP
	CurrencySystemCCMSERVPÚBLICOSSP
	CurrencySystemCREDISISCENTRALDECOOPERATIVASDECRÉDITOLTDA
	CurrencySystemCCMDESPTRÂNSSCERS
	CurrencySystemBCOAFINZSABM
	CurrencySystemCECMSERVPUBLPINHÃO
	CurrencySystemPORTOSEGSACFI
	CurrencySystemBANCOINBURSA
	CurrencySystemBCODAAMAZONIASA
	CurrencySystemCONFIDENCECCSA
	CurrencySystemBCODOESTDOPASA
	CurrencySystemVIACERTAFINANCIADORASACFI
	CurrencySystemZEMACFISA
	CurrencySystemCASACREDITOSASCM
	CurrencySystemCOOPCENTRALAILOS
	CurrencySystemCOOPCREDITAG
	CurrencySystemCREDIARECFISA
	CurrencySystemPLANNERSOCIEDADEDECRÉDITODIRETO
	CurrencySystemCENTRALCOOPERATIVADECRÉDITONOESTADODOESPÍRITOSANTO
	CurrencySystemCECMFABRICCALÇADOSSAPIRANGA
	CurrencySystemBCOBBISA
	CurrencySystemLIGAINVESTDTVMLTDA
	CurrencySystemBCOBRADESCOFINANCSA
	CurrencySystemBCODONORDESTEDOBRASILSA
	CurrencySystemHEDGEINVESTMENTSDTVMLTDA
	CurrencySystemBCOCCBBRASILSA
	CurrencySystemHSFINANCEIRA
	CurrencySystemLECCACFISA
	CurrencySystemBCOKDBBRASILSA
	CurrencySystemHSCMSCMEPPLTDA
	CurrencySystemVALORSCDSA
	CurrencySystemPOLOCREDSCMEPPLTDA
	CurrencySystemCCRDEIBIAM
	CurrencySystemCCRDESÃOMIGUELDOOESTE
	CurrencySystemBCOCSFSA
	CurrencySystemPAGSEGUROINTERNETIPSA
	CurrencySystemMONEYCORPBCODECÂMBIOSA
	CurrencySystemFDGOLDDTVMLTDA
	CurrencySystemEFÍSAIP
	CurrencySystemICAPDOBRASILCTVMLTDA
	CurrencySystemSOCREDSASCMEPP
	CurrencySystemSTATESTREETBRSABCOCOMERCIAL
	CurrencySystemCARUANASCFI
	CurrencySystemMIDWAYSASCFI
	CurrencySystemCODEPECVCSA
	CurrencySystemPICPAYBANKBANCOMÚLTIPLOSA
	CurrencySystemMASTERBISA
	CurrencySystemSUPERDIGITALIPSA
	CurrencySystemBANCOSEGUROSA
	CurrencySystemBCOYAMAHAMOTORSA
	CurrencySystemCRESOLCONFEDERAÇÃO
	CurrencySystemBCOAGIBANKSA
	CurrencySystemBCODACHINABRASILSA
	CurrencySystemGETMONEYCCLTDA
	CurrencySystemBCOBANDEPESA
	CurrencySystemGLOBALSCMLTDA
	CurrencySystemNEONFINANCEIRACFISA
	CurrencySystemBANCORANDONSA
	CurrencySystemOMDTVMLTDA
	CurrencySystemBMPSCMEPPLTDA
	CurrencySystemTRAVELEXBANCODECÂMBIOSA
	CurrencySystemBANCOFINAXIS
	CurrencySystemGAZINCREDSASCFI
	CurrencySystemBCOSENFFSA
	CurrencySystemMIRAEASSETCCTVMLTDA
	CurrencySystemBCODOESTDESESA
	CurrencySystemBEXSBCODECAMBIOSA
	CurrencySystemACESSOSOLUÇÕESDEPAGAMENTOSAINSTITUIÇÃODEPAGAMENTO
	CurrencySystemFITBANKIP
	CurrencySystemBRPARTNERSBI
	CurrencySystemÓRAMADTVMSA
	CurrencySystemDOCKIPSA
	CurrencySystemBRLTRUSTDTVMSA
	CurrencySystemFRAMCAPITALDTVMSA
	CurrencySystemBCOWESTERNUNION
	CurrencySystemHUBIPSA
	CurrencySystemCELCOINIPSA
	CurrencySystemCAMBIONETCCLTDA
	CurrencySystemPARANABCOSA
	CurrencySystemBARICIAHIPOTECÁRIA
	CurrencySystemIUGUIPSA
	CurrencySystemBCOBOCOMBBMSA
	CurrencySystemBANCOBESASA
	CurrencySystemSOCIALBANKSA
	CurrencySystemBCOWOORIBANKDOBRASILSA
	CurrencySystemFACTASACFI
	CurrencySystemSTONEIPSA
	CurrencySystemIDCTVM
	CurrencySystemAMAZÔNIACCLTDA
	CurrencySystemBROKERBRASILCCLTDA
	CurrencySystemPINBANKIP
	CurrencySystemBCOMERCANTILDOBRASILSA
	CurrencySystemBCOITAÚBBASA
	CurrencySystemBCOTRIANGULOSA
	CurrencySystemSENSOCCVMSA
	CurrencySystemICBCDOBRASILBMSA
	CurrencySystemVIPSCCLTDA
	CurrencySystemBMSSCDSA
	CurrencySystemCREFAZSCMEPPLTDA
	CurrencySystemNUPAGAMENTOSIP
	CurrencySystemCDCSCDSA
	CurrencySystemUBSBRASILBISA
	CurrencySystemBRAZABANKSABCODECÂMBIO
	CurrencySystemLAMARASCDSA
	CurrencySystemASAASIPSA
	CurrencySystemPARMETALDTVMLTDA
	CurrencySystemNEONPAGAMENTOSSAIP
	CurrencySystemEBANXIPLTDA
	CurrencySystemCARTOSSCDSA
	CurrencySystemVORTXDTVMLTDA
	CurrencySystemPICPAY
	CurrencySystemWILLFINANCEIRASACFI
	CurrencySystemGUITTACCLTDA
	CurrencySystemFFASCMEPPLTDA
	CurrencySystemCOOPDEPRIMAVERADOLESTE
	CurrencySystemBANCODIGIO
	CurrencySystemAL5SACFI
	CurrencySystemCREDUFES
	CurrencySystemREALIZECFISA
	CurrencySystemGENIALINVESTIMENTOSCVMSA
	CurrencySystemIBCCTVMSA
	CurrencySystemBCOBANESTESSA
	CurrencySystemBCOABCBRASILSA
	CurrencySystemBS2DTVMSA
	CurrencySystemBalcãoB3
	CurrencySystemCIPSAC3
	CurrencySystemSCOTIABANKBRASIL
	CurrencySystemTOROCTVMSA
	CurrencySystemNUFINANCEIRASACFI
	CurrencySystemBCOMODALSA
	CurrencySystemU4CINSTITUIÇÃODEPAGAMENTOSA
	CurrencySystemBCOCLASSICOSA
	CurrencySystemIDEALCTVMSA
	CurrencySystemBCOC6SA
	CurrencySystemBCOGUANABARASA
	CurrencySystemBCOINDUSTRIALDOBRASILSA
	CurrencySystemBCOCREDITSUISSESA
	CurrencySystemQISCDSA
	CurrencySystemFAIRCCSA
	CurrencySystemCREDITASSCD
	CurrencySystemBCOLANACIONARGENTINA
	CurrencySystemCITIBANKNA
	CurrencySystemBCOCEDULASA
	CurrencySystemBCOBRADESCOBERJSA
	CurrencySystemBCOJPMORGANSA
	CurrencySystemBCOXPSA
	CurrencySystemBCOCAIXAGERALBRASILSA
	CurrencySystemBCOCITIBANKSA
	CurrencySystemBCORODOBENSSA
	CurrencySystemBCOFATORSA
	CurrencySystemBNDES
	CurrencySystemATIVASAINVESTIMENTOSCCTVM
	CurrencySystemBGCLIQUIDEZDTVMLTDA
	CurrencySystemBANCOITAÚCONSIGNADOSA
	CurrencySystemMASTERSACCTVM
	CurrencySystemBANCOMASTER
	CurrencySystemLISTOSCDSA
	CurrencySystemHAITONGBIDOBRASILSA
	CurrencySystemINTERCAMCCLTDA
	CurrencySystemÓTIMOSCDSA
	CurrencySystemVITREODTVMSA
	CurrencySystemREAGDTVMSA
	CurrencySystemPLANTAECFI
	CurrencySystemUPPSEPSA
	CurrencySystemOLIVEIRATRUSTDTVMSA
	CurrencySystemFINVESTDTVM
	CurrencySystemQISTASACFI
	CurrencySystemBONUSPAGOSCDSA
	CurrencySystemMAFDTVMSA
	CurrencySystemCOBUCCIOSASCFI
	CurrencySystemSCFIEFÍSA
	CurrencySystemSUMUPSCDSA
	CurrencySystemZIPDINSCDSA
	CurrencySystemLENDSCDSA
	CurrencySystemDM
	CurrencySystemMERCADOCRÉDITOSCFISA
	CurrencySystemACCREDITOSCDSA
	CurrencySystemCORASCDSA
	CurrencySystemNUMBRSSCDSA
	CurrencySystemDELCREDSCDSA
	CurrencySystemFÊNIXDTVMLTDA
	CurrencySystemCCLARCREDI
	CurrencySystemCREDIHOMESCD
	CurrencySystemMARUSCDSA
	CurrencySystemUY3SCDSA
	CurrencySystemCREDSYSTEMSCDSA
	CurrencySystemHEMERADTVMLTDA
	CurrencySystemCREDIFITSCDSA
	CurrencySystemFFCREDSCDSA
	CurrencySystemSTARKSCDSA
	CurrencySystemCAPITALCONSIGSCDSA
	CurrencySystemPORTOPARDTVMLTDA
	CurrencySystemAZUMIDTVM
	CurrencySystemJ17SCDSA
	CurrencySystemTRINUSSCDSA
	CurrencySystemLIONSTRUSTDTVM
	CurrencySystemMÉRITODTVMLTDA
	CurrencySystemUNAVANTISCDSA
	CurrencySystemRJI
	CurrencySystemSBCASHSCD
	CurrencySystemBNYMELLONBCOSA
	CurrencySystemPEFISASACFI
	CurrencySystemSUPERLÓGICASCDSA
	CurrencySystemPEAKSEPSA
	CurrencySystemBRCAPITALDTVMSA
	CurrencySystemBCOLAPROVINCIABAIRESBCE
	CurrencySystemHRDIGITALSCD
	CurrencySystemATICCASCDSA
	CurrencySystemMAGNUMSCD
	CurrencySystemATFCREDITSCDSA
	CurrencySystemBANCOGENIAL
	CurrencySystemEAGLESCDSA
	CurrencySystemMICROCASHSCMEPPLTDA
	CurrencySystemWNTCAPITALDTVM
	CurrencySystemMONETARIESCD
	CurrencySystemJPMORGANCHASEBANK
	CurrencySystemREDSCDSA
	CurrencySystemSERFINANCESCDSA
	CurrencySystemBCOANDBANKSA
	CurrencySystemLEVYCAMCCVLTDA
	CurrencySystemBCVBCOCREDITOEVAREJOSA
	CurrencySystemBEXSCCSA
	CurrencySystemBCOHSBCSA
	CurrencySystemBCOARBISA
	CurrencySystemCâmaraB3
	CurrencySystemINTESASANPAOLOBRASILSABM
	CurrencySystemBCOTRICURYSA
	CurrencySystemBCOSAFRASA
	CurrencySystemBCOLETSBANKSA
	CurrencySystemBCOFIBRASA
	CurrencySystemBCOVOLKSWAGENSA
	CurrencySystemBCOLUSOBRASILEIROSA
	CurrencySystemBCOGMSA
	CurrencySystemBANCOPAN
	CurrencySystemBCOVOTORANTIMSA
	CurrencySystemBCOITAUBANKSA
	CurrencySystemBCOMUFGBRASILSA
	CurrencySystemBCOSUMITOMOMITSUIBRASILSA
	CurrencySystemITAÚUNIBANCOSA
	CurrencySystemBCOBRADESCOSA
	CurrencySystemBCOMERCEDESBENZSA
	CurrencySystemOMNIBANCOSA
	CurrencySystemBCOSOFISASA
	CurrencySystemCâmbioB3
	CurrencySystemBANCOVOITER
	CurrencySystemBCOCREFISASA
	CurrencySystemBCOMIZUHOSA
	CurrencySystemBANCOINVESTCREDUNIBANCOSA
	CurrencySystemBCOBMGSA
	CurrencySystemBCOC6CONSIG
	CurrencySystemAVENUESECURITIESDTVMLTDA
	CurrencySystemSAGITURCC
	CurrencySystemBCOSOCIETEGENERALEBRASIL
	CurrencySystemNEONCTVMSA
	CurrencySystemTULLETTPREBONBRASILCVCLTDA
	CurrencySystemCSUISSEHEDGINGGRIFFOCVSA
	CurrencySystemBCOPAULISTASA
	CurrencySystemBOFAMERRILLLYNCHBMSA
	CurrencySystemCREDISANCC
	CurrencySystemBCOPINESA
	CurrencySystemNUINVESTCORRETORADEVALORESSA
	CurrencySystemBCODAYCOVALSA
	CurrencySystemCAROLDTVMLTDA
	CurrencySystemSINGULARECTVMSA
	CurrencySystemRENASCENCADTVMLTDA
	CurrencySystemDEUTSCHEBANKSABCOALEMAO
	CurrencySystemBANCOCIFRA
	CurrencySystemGUIDE
	CurrencySystemTRUSTEEDTVMLTDA
	CurrencySystemSIMPAUL
	CurrencySystemBCORENDIMENTOSA
	CurrencySystemBCOBS2SA
	CurrencySystemLASTRORDVDTVMLTDA
	CurrencySystemFRENTECCSA
	CurrencySystemBTCCLTDA
	CurrencySystemNOVOBCOCONTINENTALSABM
	CurrencySystemBCOCRÉDITAGRICOLEBRSA
	CurrencySystemCCRCOOPAVEL
	CurrencySystemBANCOSISTEMA
	CurrencySystemDOURADACORRETORA
	CurrencySystemCREDIALIANÇACCR
	CurrencySystemBCOVRSA
	CurrencySystemBCOOURINVESTSA
	CurrencySystemBCORNXSA
	CurrencySystemCREDICOAMO
	CurrencySystemCREDIBRFCOOP
	CurrencySystemMAGNETISDTVM
	CurrencySystemRBINVESTIMENTOSDTVMLTDA
	CurrencySystemBCOSANTANDERBRASILSA
	CurrencySystemBANCOJOHNDEERESA
	CurrencySystemBCODOESTADODORSSA
	CurrencySystemADVANCEDCCLTDA
	CurrencySystemBCODIGIMAISSA
	CurrencySystemWARRENCVMCLTDA
	CurrencySystemBANCOORIGINAL
	CurrencySystemEFXCCLTDA
	CurrencySystemCashwayDigital
)

func (s CurrencySystem) String() string {
	currencies := []string{"unknown", "bitcoin", "ethereum", "polygon", "brazzilian-bank", "btg-bank", "topazio-bank", "mercado-pago-bank", "tron", "BRB - BCO DE BRASILIA S.A.", "Selic", "Bacen", "SANTINVEST S.A. - CFI", "CCR SEARA", "AGK CC S.A.", "CONF NAC COOP CENTRAIS UNICRED", "ÍNDIGO INVESTIMENTOS DTVM LTDA.", "CAIXA ECONOMICA FEDERAL", "STN", "BANCO INTER", "COLUNA S.A. DTVM", "BCO RIBEIRAO PRETO S.A.", "BANCO BARI S.A.", "BCO CETELEM S.A.", "EWALLY IP S.A.", "BANCO SEMEAR", "PLANNER CV S.A.", "FDO GARANTIDOR CRÉDITOS", "BCO B3 S.A.", "BCO RABOBANK INTL BRASIL S.A.", "CIELO IP S.A.", "CCR DE ABELARDO LUZ", "BCO COOPERATIVO SICREDI S.A.", "CREHNOR LARANJEIRAS", "BCO BNP PARIBAS BRASIL S A", "CECM COOPERFORTE", "KIRTON BANK", "BCO BRASILEIRO DE CRÉDITO S.A.", "BCO BV S.A.", "BANCO SICOOB S.A.", "TRINUS CAPITAL DTVM", "BCO KEB HANA DO BRASIL S.A.", "XP INVESTIMENTOS CCTVM S/A", "SISPRIME DO BRASIL - COOP", "CM CAPITAL MARKETS CCTVM LTDA", "BCO MORGAN STANLEY S.A.", "UBS BRASIL CCTVM S.A.", "TREVISO CC S.A.", "CIP SA Siloc", "HIPERCARD BM S.A.", "BCO. J.SAFRA S.A.", "UNIPRIME COOPCENTRAL LTDA.", "BCO TOYOTA DO BRASIL S.A.", "PARATI - CFI S.A.", "BCO ALFA S.A.", "BCO ABN AMRO S.A.", "BCO CARGILL S.A.", "TERRA INVESTIMENTOS DTVM", "CECM DOS TRAB.PORT. DA G.VITOR", "SOCINAL S.A. CFI", "SERVICOOP", "OZ CORRETORA DE CÂMBIO S.A.", "BANCO BRADESCARD", "NOVA FUTURA CTVM LTDA.", "FIDUCIA SCMEPP LTDA", "GOLDMAN SACHS DO BRASIL BM S.A", "CAMARA INTERBANCARIA DE PAGAMENTOS - CIP", "CCM SERV. PÚBLICOS SP", "CREDISIS CENTRAL DE COOPERATIVAS DE CRÉDITO LTDA.", "CCM DESP TRÂNS SC E RS", "BCO AFINZ S.A. - BM", "CECM SERV PUBL PINHÃO", "PORTOSEG S.A. CFI", "BANCO INBURSA", "BCO DA AMAZONIA S.A.", "CONFIDENCE CC S.A.", "BCO DO EST. DO PA S.A.", "VIA CERTA FINANCIADORA S.A. - CFI", "ZEMA CFI S/A", "CASA CREDITO S.A. SCM", "COOPCENTRAL AILOS", "COOP CREDITAG", "CREDIARE CFI S.A.", "PLANNER SOCIEDADE DE CRÉDITO DIRETO", "CENTRAL COOPERATIVA DE CRÉDITO NO ESTADO DO ESPÍRITO SANTO", "CECM FABRIC CALÇADOS SAPIRANGA", "BCO BBI S.A.", "LIGA INVEST DTVM LTDA.", "BCO BRADESCO FINANC. S.A.", "BCO DO NORDESTE DO BRASIL S.A.", "HEDGE INVESTMENTS DTVM LTDA.", "BCO CCB BRASIL S.A.", "HS FINANCEIRA", "LECCA CFI S.A.", "BCO KDB BRASIL S.A.", "HSCM SCMEPP LTDA.", "VALOR SCD S.A.", "POLOCRED SCMEPP LTDA.", "CCR DE IBIAM", "CCR DE SÃO MIGUEL DO OESTE", "BCO CSF S.A.", "PAGSEGURO INTERNET IP S.A.", "MONEYCORP BCO DE CÂMBIO S.A.", "F D GOLD DTVM LTDA", "EFÍ S.A. - IP", "ICAP DO BRASIL CTVM LTDA.", "SOCRED SA - SCMEPP", "STATE STREET BR S.A. BCO COMERCIAL", "CARUANA SCFI", "MIDWAY S.A. - SCFI", "CODEPE CVC S.A.", "PICPAY BANK - BANCO MÚLTIPLO S.A", "MASTER BI S.A.", "SUPERDIGITAL I.P. S.A.", "BANCOSEGURO S.A.", "BCO YAMAHA MOTOR S.A.", "CRESOL CONFEDERAÇÃO", "BCO AGIBANK S.A.", "BCO DA CHINA BRASIL S.A.", "GET MONEY CC LTDA", "BCO BANDEPE S.A.", "GLOBAL SCM LTDA", "NEON FINANCEIRA - CFI S.A.", "BANCO RANDON S.A.", "OM DTVM LTDA", "BMP SCMEPP LTDA", "TRAVELEX BANCO DE CÂMBIO S.A.", "BANCO FINAXIS", "GAZINCRED S.A. SCFI", "BCO SENFF S.A.", "MIRAE ASSET CCTVM LTDA", "BCO DO EST. DE SE S.A.", "BEXS BCO DE CAMBIO S.A.", "ACESSO SOLUÇÕES DE PAGAMENTO S.A. - INSTITUIÇÃO DE PAGAMENTO", "FITBANK IP", "BR PARTNERS BI", "ÓRAMA DTVM S.A.", "DOCK IP S.A.", "BRL TRUST DTVM SA", "FRAM CAPITAL DTVM S.A.", "BCO WESTERN UNION", "HUB IP S.A.", "CELCOIN IP S.A.", "CAMBIONET CC LTDA", "PARANA BCO S.A.", "BARI CIA HIPOTECÁRIA", "IUGU IP S.A.", "BCO BOCOM BBM S.A.", "BANCO BESA S.A.", "SOCIAL BANK S/A", "BCO WOORI BANK DO BRASIL S.A.", "FACTA S.A. CFI", "STONE IP S.A.", "ID CTVM", "AMAZÔNIA CC LTDA.", "BROKER BRASIL CC LTDA.", "PINBANK IP", "BCO MERCANTIL DO BRASIL S.A.", "BCO ITAÚ BBA S.A.", "BCO TRIANGULO S.A.", "SENSO CCVM S.A.", "ICBC DO BRASIL BM S.A.", "VIPS CC LTDA.", "BMS SCD S.A.", "CREFAZ SCMEPP LTDA", "NU PAGAMENTOS - IP", "CDC SCD S.A.", "UBS BRASIL BI S.A.", "BRAZA BANK S.A. BCO DE CÂMBIO", "LAMARA SCD S.A.", "ASAAS IP S.A.", "PARMETAL DTVM LTDA", "NEON PAGAMENTOS S.A. IP", "EBANX IP LTDA.", "CARTOS SCD S.A.", "VORTX DTVM LTDA.", "PICPAY", "WILL FINANCEIRA S.A.CFI", "GUITTA CC LTDA", "FFA SCMEPP LTDA.", "COOP DE PRIMAVERA DO LESTE", "BANCO DIGIO", "AL5 S.A. CFI", "CRED-UFES", "REALIZE CFI S.A.", "GENIAL INVESTIMENTOS CVM S.A.", "IB CCTVM S.A.", "BCO BANESTES S.A.", "BCO ABC BRASIL S.A.", "BS2 DTVM S.A.", "Balcão B3", "CIP SA C3", "SCOTIABANK BRASIL", "TORO CTVM S.A.", "NU FINANCEIRA S.A. CFI", "BCO MODAL S.A.", "U4C INSTITUIÇÃO DE PAGAMENTO S.A.", "BCO CLASSICO S.A.", "IDEAL CTVM S.A.", "BCO C6 S.A.", "BCO GUANABARA S.A.", "BCO INDUSTRIAL DO BRASIL S.A.", "BCO CREDIT SUISSE S.A.", "QI SCD S.A.", "FAIR CC S.A.", "CREDITAS SCD", "BCO LA NACION ARGENTINA", "CITIBANK N.A.", "BCO CEDULA S.A.", "BCO BRADESCO BERJ S.A.", "BCO J.P. MORGAN S.A.", "BCO XP S.A.", "BCO CAIXA GERAL BRASIL S.A.", "BCO CITIBANK S.A.", "BCO RODOBENS S.A.", "BCO FATOR S.A.", "BNDES", "ATIVA S.A. INVESTIMENTOS CCTVM", "BGC LIQUIDEZ DTVM LTDA", "BANCO ITAÚ CONSIGNADO S.A.", "MASTER S/A CCTVM", "BANCO MASTER", "LISTO SCD S.A.", "HAITONG BI DO BRASIL S.A.", "INTERCAM CC LTDA", "ÓTIMO SCD S.A.", "VITREO DTVM S.A.", "REAG DTVM S.A.", "PLANTAE CFI", "UP.P SEP S.A.", "OLIVEIRA TRUST DTVM S.A.", "FINVEST DTVM", "QISTA S.A. CFI", "BONUSPAGO SCD S.A.", "MAF DTVM SA", "COBUCCIO S.A. SCFI", "SCFI EFÍ S.A.", "SUMUP SCD S.A.", "ZIPDIN SCD S.A.", "LEND SCD S.A.", "DM", "MERCADO CRÉDITO SCFI S.A.", "ACCREDITO SCD S.A.", "CORA SCD S.A.", "NUMBRS SCD S.A.", "DELCRED SCD S.A.", "FÊNIX DTVM LTDA.", "CC LAR CREDI", "CREDIHOME SCD", "MARU SCD S.A.", "UY3 SCD S/A", "CREDSYSTEM SCD S.A.", "HEMERA DTVM LTDA.", "CREDIFIT SCD S.A.", "FFCRED SCD S.A.", "STARK SCD S.A.", "CAPITAL CONSIG SCD S.A.", "PORTOPAR DTVM LTDA", "AZUMI DTVM", "J17 - SCD S/A", "TRINUS SCD S.A.", "LIONS TRUST DTVM", "MÉRITO DTVM LTDA.", "UNAVANTI SCD S/A", "RJI", "SBCASH SCD", "BNY MELLON BCO S.A.", "PEFISA S.A. - C.F.I.", "SUPERLÓGICA SCD S.A.", "PEAK SEP S.A.", "BR-CAPITAL DTVM S.A.", "BCO LA PROVINCIA B AIRES BCE", "HR DIGITAL SCD", "ATICCA SCD S.A.", "MAGNUM SCD", "ATF CREDIT SCD S.A.", "BANCO GENIAL", "EAGLE SCD S.A.", "MICROCASH SCMEPP LTDA.", "WNT CAPITAL DTVM", "MONETARIE SCD", "JPMORGAN CHASE BANK", "RED SCD S.A.", "SER FINANCE SCD S.A.", "BCO ANDBANK S.A.", "LEVYCAM CCV LTDA", "BCV - BCO, CRÉDITO E VAREJO S.A.", "BEXS CC S.A.", "BCO HSBC S.A.", "BCO ARBI S.A.", "Câmara B3", "INTESA SANPAOLO BRASIL S.A. BM", "BCO TRICURY S.A.", "BCO SAFRA S.A.", "BCO LETSBANK S.A.", "BCO FIBRA S.A.", "BCO VOLKSWAGEN S.A", "BCO LUSO BRASILEIRO S.A.", "BCO GM S.A.", "BANCO PAN", "BCO VOTORANTIM S.A.", "BCO ITAUBANK S.A.", "BCO MUFG BRASIL S.A.", "BCO SUMITOMO MITSUI BRASIL S.A.", "ITAÚ UNIBANCO S.A.", "BCO BRADESCO S.A.", "BCO MERCEDES-BENZ S.A.", "OMNI BANCO S.A.", "BCO SOFISA S.A.", "Câmbio B3", "BANCO VOITER", "BCO CREFISA S.A.", "BCO MIZUHO S.A.", "BANCO INVESTCRED UNIBANCO S.A.", "BCO BMG S.A.", "BCO C6 CONSIG", "AVENUE SECURITIES DTVM LTDA.", "SAGITUR CC", "BCO SOCIETE GENERALE BRASIL", "NEON CTVM S.A.", "TULLETT PREBON BRASIL CVC LTDA", "C.SUISSE HEDGING-GRIFFO CV S/A", "BCO PAULISTA S.A.", "BOFA MERRILL LYNCH BM S.A.", "CREDISAN CC", "BCO PINE S.A.", "NU INVEST CORRETORA DE VALORES S.A.", "BCO DAYCOVAL S.A", "CAROL DTVM LTDA.", "SINGULARE CTVM S.A.", "RENASCENCA DTVM LTDA", "DEUTSCHE BANK S.A.BCO ALEMAO", "BANCO CIFRA", "GUIDE", "TRUSTEE DTVM LTDA.", "SIMPAUL", "BCO RENDIMENTO S.A.", "BCO BS2 S.A.", "LASTRO RDV DTVM LTDA", "FRENTE CC S.A.", "B&T CC LTDA.", "NOVO BCO CONTINENTAL S.A. - BM", "BCO CRÉDIT AGRICOLE BR S.A.", "CCR COOPAVEL", "BANCO SISTEMA", "DOURADA CORRETORA", "CREDIALIANÇA CCR", "BCO VR S.A.", "BCO OURINVEST S.A.", "BCO RNX S.A.", "CREDICOAMO", "CREDIBRF COOP", "MAGNETIS - DTVM", "RB INVESTIMENTOS DTVM LTDA.", "BCO SANTANDER (BRASIL) S.A.", "BANCO JOHN DEERE S.A.", "BCO DO ESTADO DO RS S.A.", "ADVANCED CC LTDA", "BCO DIGIMAIS S.A.", "WARREN CVMC LTDA", "BANCO ORIGINAL", "EFX CC LTDA.", "cashway-bank"}
	if int(s) > len(currencies) {
		return currencies[0]
	}
	return currencies[s]
}

var (
	BrazilianBankNumberToCurrencySystem = map[string]int{
		"001": 4, "323": 7, "082": 6, "208": 5, "070": 9, "Selic": 10, "Bacen": 11, "539": 12, "430": 13, "272": 14, "136": 15, "407": 16, "104": 17, "STN": 18, "077": 19, "423": 20, "741": 21, "330": 22, "739": 23, "534": 24, "743": 25, "100": 26, "541": 27, "096": 28, "747": 29, "362": 30, "322": 31, "748": 32, "350": 33, "752": 34, "379": 35, "399": 36, "378": 37, "413": 38, "756": 39, "360": 40, "757": 41, "102": 42, "084": 43, "180": 44, "066": 45, "015": 46, "143": 47, "CIP SA Siloc": 48, "062": 49, "074": 50, "099": 51, "387": 52, "326": 53, "025": 54, "075": 55, "040": 56, "307": 57, "385": 58, "425": 59, "190": 60, "296": 61, "063": 62, "191": 63, "382": 64, "064": 65, "CAMARA INTERBANCARIA DE PAGAMENTOS - CIP": 66, "459": 67, "097": 68, "016": 69, "299": 70, "471": 71, "468": 72, "012": 73, "003": 74, "060": 75, "037": 76, "411": 77, "359": 78, "159": 79, "085": 80, "400": 81, "429": 82, "410": 83, "114": 84, "328": 85, "036": 86, "469": 87, "394": 88, "004": 89, "458": 90, "320": 91, "189": 92, "105": 93, "076": 94, "312": 95, "195": 96, "093": 97, "391": 98, "273": 99, "368": 100, "290": 101, "259": 102, "395": 103, "364": 104, "157": 105, "183": 106, "014": 107, "130": 108, "358": 109, "127": 110, "079": 111, "141": 112, "340": 113, "081": 114, "475": 115, "133": 116, "121": 117, "083": 118, "138": 119, "024": 120, "384": 121, "426": 122, "088": 123, "319": 124, "274": 125, "095": 126, "094": 127, "478": 128, "276": 129, "447": 130, "047": 131, "144": 132, "332": 133, "450": 134, "126": 135, "325": 136, "301": 137, "173": 138, "331": 139, "119": 140, "396": 141, "509": 142, "309": 143, "254": 144, "268": 145, "401": 146, "107": 147, "334": 148, "412": 149, "124": 150, "149": 151, "197": 152, "439": 153, "313": 154, "142": 155, "529": 156, "389": 157, "184": 158, "634": 159, "545": 160, "132": 161, "298": 162, "377": 163, "321": 164, "260": 165, "470": 166, "129": 167, "128": 168, "416": 169, "461": 170, "194": 171, "536": 172, "383": 173, "324": 174, "310": 175, "380": 176, "280": 177, "146": 178, "343": 179, "279": 180, "335": 181, "349": 182, "427": 183, "374": 184, "278": 185, "271": 186, "021": 187, "246": 188, "292": 189, "Balcão B3": 190, "CIP SA C3": 191, "751": 192, "352": 193, "386": 194, "746": 195, "546": 196, "241": 197, "398": 198, "336": 199, "612": 200, "604": 201, "505": 202, "329": 203, "196": 204, "342": 205, "300": 206, "477": 207, "266": 208, "122": 209, "376": 210, "348": 211, "473": 212, "745": 213, "120": 214, "265": 215, "007": 216, "188": 217, "134": 218, "029": 219, "467": 220, "243": 221, "397": 222, "078": 223, "525": 224, "355": 225, "367": 226, "528": 227, "445": 228, "373": 229, "111": 230, "512": 231, "516": 232, "408": 233, "484": 234, "402": 235, "507": 236, "404": 237, "418": 238, "414": 239, "449": 240, "518": 241, "406": 242, "403": 243, "419": 244, "435": 245, "455": 246, "421": 247, "443": 248, "535": 249, "457": 250, "428": 251, "448": 252, "452": 253, "510": 254, "462": 255, "465": 256, "306": 257, "463": 258, "451": 259, "444": 260, "519": 261, "454": 262, "460": 263, "506": 264, "482": 265, "017": 266, "174": 267, "481": 268, "521": 269, "433": 270, "495": 271, "523": 272, "527": 273, "511": 274, "513": 275, "125": 276, "532": 277, "537": 278, "524": 279, "526": 280, "488": 281, "522": 282, "530": 283, "065": 284, "145": 285, "250": 286, "253": 287, "269": 288, "213": 289, "Câmara B3": 290, "139": 291, "018": 292, "422": 293, "630": 294, "224": 295, "393": 296, "600": 297, "390": 298, "623": 299, "655": 300, "479": 301, "456": 302, "464": 303, "341": 304, "237": 305, "381": 306, "613": 307, "637": 308, "Câmbio B3": 309, "653": 310, "069": 311, "370": 312, "249": 313, "318": 314, "626": 315, "508": 316, "270": 317, "366": 318, "113": 319, "131": 320, "011": 321, "611": 322, "755": 323, "089": 324, "643": 325, "140": 326, "707": 327, "288": 328, "363": 329, "101": 330, "487": 331, "233": 332, "177": 333, "438": 334, "365": 335, "633": 336, "218": 337, "293": 338, "285": 339, "080": 340, "753": 341, "222": 342, "281": 343, "754": 344, "311": 345, "098": 346, "610": 347, "712": 348, "720": 349, "010": 350, "440": 351, "442": 352, "283": 353, "033": 354, "217": 355, "041": 356, "117": 357, "654": 358, "371": 359, "212": 360, "289": 361,
	}
)

func BankNumberCurrencySystem(banknumber string) CurrencySystem {
	currencySystem, ok := BrazilianBankNumberToCurrencySystem[banknumber]
	if ok {
		return CurrencySystem(currencySystem)
	} else {
		return CurrencySystemUnknow
	}
}

type EnumEventOrigin int

const (
	EventOriginUnknown EnumEventOrigin = iota
	EventOriginStatement
	EventOriginPix
	EventOriginBoleto
	EventOriginTED
	EventOriginChargePix
)

func (s EnumEventOrigin) String() string {
	return []string{"origin-unknown", "origin-statement", "origin-pix", "origin-boleto", "origin-ted", "origin-charge-pix"}[s]
}

type EventType int

const (
	EventPaymentTypeUnknown EventType = iota
	EventPaymentGenericTransfer
	EventPaymentBoleto
	EventPaymentPix
	EventPaymentTED
	EventPaymentBlokchain
)

func (s EventType) String() string {
	return []string{"payment-type-unknown", "payment-generic", "payment-boleto", "payment-pix", "payment-ted", "payment-blockchain"}[s]
}

func StringEventType(t string) EventType {
	strtypes := map[string]EventType{
		"pix":           EventPaymentPix,
		"boleto":        EventPaymentBoleto,
		"ted":           EventPaymentTED,
		"blockchain":    EventPaymentBlokchain,
		"bank-transfer": EventPaymentGenericTransfer,
	}

	c, ok := strtypes[t]
	if ok {
		return c
	}

	return EventPaymentGenericTransfer
}

type Currency int

const (
	CurrencyUnknown Currency = iota
	CurrencyFiatBRL
	CurrencyFiatUSD
	CurrencyBTC
	CurrencyETH
	CurrencyTRX
	CurrencyUSDT
)

var (
	CurrencyLabels = map[string]Currency{
		"unknown": CurrencyUnknown,
		"fiatBRL": CurrencyFiatBRL,
		"fiatUSD": CurrencyFiatUSD,
		"BTC":     CurrencyBTC,
		"ETH":     CurrencyETH,
		"TRX":     CurrencyTRX,
		"USDT":    CurrencyUSDT,
	}
)

func (s Currency) String() string {
	return []string{"unknown", "fiatBRL", "fiatUSD", "BTC", "ETH", "TRX", "USDT"}[s]
}

func CurrencyLabel(label string) (Currency, error) {
	if _, ok := CurrencyLabels[label]; !ok {
		return CurrencyUnknown, fmt.Errorf("currency label not found")
	}
	return CurrencyLabels[label], nil
}

type AllowedCurrencyPair string

var (
	AllowedCurrencyPairs = []string{
		fmt.Sprintf("%d:%d", CurrencyFiatBRL, CurrencyUSDT),
		fmt.Sprintf("%d:%d", CurrencyUSDT, CurrencyFiatBRL),
		fmt.Sprintf("%d:%d", CurrencyBTC, CurrencyFiatBRL),
		fmt.Sprintf("%d:%d", CurrencyFiatBRL, CurrencyBTC),
	}
)

func CurrencyPairFromLabel(pair string) (string, error) {
	splitpair := strings.Split(pair, ":")
	if len(splitpair) < 2 {
		return "", fmt.Errorf("requested pair is not valid")
	}
	leftSide, err := CurrencyLabel(splitpair[0])
	if err != nil {
		return "", err
	}
	rightSide, err := CurrencyLabel(splitpair[1])
	if err != nil {
		return "", err
	}
	currencypair := fmt.Sprintf("%d:%d", leftSide, rightSide)
	return currencypair, nil
}

func ValidateAllowedCurrencyPair(pair AllowedCurrencyPair) error {
	if slices.Contains(AllowedCurrencyPairs, pair.CurrencyTypePair()) {
		return nil
	}
	return fmt.Errorf("pair not found for payments")
}

func (s AllowedCurrencyPair) String() string {
	left, right := s.LeftSideCurrency().String(), s.RightSideCurrency().String()
	return left + ":" + right
}

func (s AllowedCurrencyPair) CurrencyTypePair() string {
	return fmt.Sprintf("%d:%d", s.LeftSideCurrency(), s.RightSideCurrency())
}

func (s AllowedCurrencyPair) LeftSideCurrency() Currency {
	c, _ := strconv.Atoi(strings.Split(string(s), ":")[0])
	return Currency(c)
}

func (s AllowedCurrencyPair) RightSideCurrency() Currency {
	c, _ := strconv.Atoi(strings.Split(string(s), ":")[1])
	return Currency(c)
}
