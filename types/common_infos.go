package types

type Contact struct {
	Emails []string `json:"emails,omitempty" bson:"emails,omitempty"`
	Phones []string `json:"phones,omitempty" bson:"phones,omitempty"`
}

func (s Contact) FirstEmail() string {
	if len(s.Emails) > 0 {
		return s.Emails[0]
	}
	return ""
}

func (s Contact) FirstPhone() string {
	if len(s.Phones) > 0 {
		return s.Phones[0]
	}
	return ""
}
