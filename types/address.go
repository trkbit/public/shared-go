package types

type Address struct {
	ZipCode    string `json:"zip_code" bson:"zip_code"`
	Address    string `json:"address" bson:"address"`
	Number     string `json:"number" bson:"number"`
	Complement any    `json:"complement" bson:"complement"`
	District   string `json:"district" bson:"district"`
	City       string `json:"city" bson:"city"`
	State      string `json:"state" bson:"state"`
	Country    string `json:"country" bson:"country"`
}
