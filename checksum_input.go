package trkbitsharedgo

import (
	"crypto/sha256"
	"fmt"

	"gitlab.com/trkbit/public/shared-go/types"
)

type InputSum []byte

func (s InputSum) String() string {
	return string(s)
}

func (s InputSum) Hash() string {
	hash := fmt.Sprintf("%x", s)
	return hash
}

func InputCheckSum(currencysystem types.CurrencySystem, uniqueid string, date string, amount int) InputSum {
	data := fmt.Sprintf("%d|%s|%s|%d", currencysystem, uniqueid, date, amount)
	hasher := sha256.New()
	_, err := hasher.Write([]byte(data))
	if err != nil {
		panic(err)
	}
	result := hasher.Sum(nil)
	return result
}
