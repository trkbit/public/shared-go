package config

import (
	"log"
	"os"
	"testing"

	"github.com/magiconair/properties/assert"
)

func TestInitiatorConfig(t *testing.T) {
	// check default config
	t.Run("success default initialization", func(t *testing.T) {
		GenerateConfig()
		assert.Equal(t, stateConfig.Env, "stg", "default initialization test failed")
	})

	t.Run("get file from environment variable and generate config", func(t *testing.T) {
		os.Setenv("CONFIG_FILE", "./stub/config.yaml")
		GenerateConfig()
		log.Println(Security.Tls.JwtPrivateKey)
		assert.Equal(t, stateConfig.Env, "dev", "environment file generate config test failed")
	})

}
