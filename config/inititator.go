package config

import (
	"log"
	"os"

	"github.com/spf13/viper"
)

var (
	CONFIG_FILE             = ""
	stateConfig     *Config = newDefaultConfig()
	Global                  = stateConfig.GlobalConfig
	CreditagAdapter         = stateConfig.CreditagBankAdapterConfig
	BbBankAdapter           = stateConfig.BbBankAdapterConfig
	CashwayAdapter          = stateConfig.CashwayBankAdapterConfig
	BtgAdapter              = stateConfig.BtgBankAdapterConfig
	Security                = stateConfig.SecurityConfig
	Broker                  = stateConfig.BrokerConfig
	Database                = stateConfig.DatabaseConfig
	Pix                     = stateConfig.PixConfig
	Identity                = stateConfig.IdentityConfig
	InvoiceManager          = stateConfig.InvoiceManagerConfig
	PaymentCore             = stateConfig.PaymentCoreConfig
	Auth                    = stateConfig.AuthConfig
	Swapper                 = stateConfig.SwapperConfig
)

func GenerateConfig() Config {
	CONFIG_FILE = os.Getenv("CONFIG_FILE")

	if CONFIG_FILE == "" {
		log.Println("environment variable config file was not found")
	} else {
		log.Println("environment variable config file found")
		v := viper.New()
		v.SetConfigFile(CONFIG_FILE)
		v.SetConfigType("yaml")
		v.AddConfigPath(".")
		err := v.ReadInConfig()
		if err != nil {
			log.Fatalf("error config file: %s \n", err)
		}

		if err := v.Unmarshal(&stateConfig); err != nil {
			log.Fatalln(err)
		}
	}

	if stateConfig.IsProduction() {
		os.Setenv("GIN_MODE", "release")
	}

	log.Println(stateConfig.Env)

	return *stateConfig
}
