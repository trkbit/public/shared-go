package config

import (
	"fmt"
	"log"
	"os"
	"reflect"

	"gopkg.in/yaml.v3"
)

type Config struct {
	*GlobalConfig              `json:"global" mapstructure:"global"`
	*IdentityConfig            `json:"identity" mapstructure:"identity"`
	*InvoiceManagerConfig      `json:"invoice_manager" mapstructure:"invoice_manager"`
	*PaymentCoreConfig         `json:"payment_core" mapstructure:"payment_core"`
	*BbBankAdapterConfig       `json:"bb_bank_adapter" mapstructure:"bb_bank_adapter"`
	*CashwayBankAdapterConfig  `json:"cashway_bank_adapter" mapstructure:"cashway_bank_adapter"`
	*BtgBankAdapterConfig      `json:"btg_bank_adapter" mapstructure:"btg_bank_adapter"`
	*CreditagBankAdapterConfig `json:"credtag_bank_adapter" mapstructure:"creditag_bank_adapter"`
	*SwapperConfig             `json:"swapper" mapstructure:"swapper"`
	*AuthConfig                `json:"auth" mapstructure:"auth"`
}

func newDefaultConfig() *Config {
	return &Config{
		GlobalConfig: &GlobalConfig{
			Env:            "stg",
			BrokerConfig:   setupDefaultBrokerConfig(),
			DatabaseConfig: setupDefaultDatabaseConfig(),
			SecurityConfig: setupDefaultSecurityConfig(),
			PixConfig:      setupDefaultPixConfig(),
		},
		IdentityConfig:            setupDefaultIdentityConfig(),
		InvoiceManagerConfig:      setupDefaultInvoiceManagerConfig(),
		PaymentCoreConfig:         setupDefaultPaymentCoreConfig(),
		BbBankAdapterConfig:       setupDefaultBbBankAdapterConfig(),
		BtgBankAdapterConfig:      setupDefaultBtgBankAdapterConfig(),
		CashwayBankAdapterConfig:  setupDefaultCashwayBankAdapterConfig(),
		CreditagBankAdapterConfig: setupDefaultCreditagBankAdapterConfig(),
		SwapperConfig:             setupDefaultSwapperConfig(),
		AuthConfig:                setupDefaultAuthConfig(),
	}
}

type GlobalConfig struct {
	Env             string `json:"env" mapstructure:"env"`
	*BrokerConfig   `json:"broker" mapstructure:"broker"`
	*DatabaseConfig `json:"database" mapstructure:"database"`
	*SecurityConfig `json:"security" mapstructure:"security"`
	*PixConfig      `json:"pix" mapstructure:"pix"`
}

func (s GlobalConfig) IsProduction() bool {
	return s.Env == "prd"
}

func (s GlobalConfig) IsStaging() bool {
	return s.Env == "stg"
}

func Make() {
	configTmpl := newDefaultConfig()
	b, _ := yaml.Marshal(configTmpl)
	err := os.WriteFile("config.mapstructure", b, 0644)
	log.Println(err)
}

type BrokerConfig struct {
	Provider string `json:"provider" mapstructure:"provider"`
	Rabbitmq struct {
		Url                string `json:"url" mapstructure:"url"`
		ExchangeNewPayment string `json:"exchange_new_payment" mapstructure:"exchange_new_payment"`
	} `json:"rabbitmq" mapstructure:"rabbitmq"`
	Nats struct {
		Url                   string `json:"url" mapstructure:"url"`
		StreamNewEventPayment string `json:"stream_new_event_payment" mapstructure:"stream_new_event_payment"`
		StreamNewPayment      string `json:"stream_new_payment" mapstructure:"stream_new_payment"`
	} `json:"nats" mapstructure:"nats"`
}

func setupDefaultBrokerConfig() *BrokerConfig {
	return &BrokerConfig{
		Provider: "rabbitmq",
		Rabbitmq: struct {
			Url                string "json:\"url\" mapstructure:\"url\""
			ExchangeNewPayment string "json:\"exchange_new_payment\" mapstructure:\"exchange_new_payment\""
		}{
			Url:                "amqp://guest:guest@localhost:5672",
			ExchangeNewPayment: "exchange_new_payment",
		},
		Nats: struct {
			Url                   string "json:\"url\" mapstructure:\"url\""
			StreamNewEventPayment string "json:\"stream_new_event_payment\" mapstructure:\"stream_new_event_payment\""
			StreamNewPayment      string "json:\"stream_new_payment\" mapstructure:\"stream_new_payment\""
		}{
			Url:                   "nats://localhost",
			StreamNewEventPayment: "events.event_payment",
			StreamNewPayment:      "events.new_payment",
		},
	}
}

type DatabaseConfig struct {
	Mongo struct {
		Url string `json:"url" mapstructure:"url"`
	} `json:"mongo" mapstructure:"mongo"`
	Mysql struct {
		Url string `json:"url" mapstructure:"url"`
	} `json:"mysql" mapstructure:"mysql"`
}

func (s DatabaseConfig) MongoUriDb(databaseName string) string {
	return fmt.Sprintf("%s/%s", s.Mongo.Url, databaseName)
}

func (s DatabaseConfig) MysqlUriDb(databaseName string) string {
	return fmt.Sprintf("%s/%s", s.Mysql.Url, databaseName)
}

func setupDefaultDatabaseConfig() *DatabaseConfig {
	return &DatabaseConfig{
		Mongo: struct {
			Url string "json:\"url\" mapstructure:\"url\""
		}{
			Url: "mongo://root:root@localhost:27017",
		},
		Mysql: struct {
			Url string "json:\"url\" mapstructure:\"url\""
		}{
			Url: "mysql://root:root@localhost:3306",
		},
	}
}

type SecurityConfig struct {
	Tls struct {
		JwtPublicKey  string `json:"jwt_public_key" mapstructure:"jwt_public_key"`
		JwtPrivateKey string `json:"jwt_private_key" mapstructure:"jwt_private_key"`
	} `json:"tls" mapstructure:"tls"`
}

func setupDefaultSecurityConfig() *SecurityConfig {
	return &SecurityConfig{
		Tls: struct {
			JwtPublicKey  string "json:\"jwt_public_key\" mapstructure:\"jwt_public_key\""
			JwtPrivateKey string "json:\"jwt_private_key\" mapstructure:\"jwt_private_key\""
		}{
			JwtPublicKey:  "/path/to/jwt/public.key",
			JwtPrivateKey: "/path/to/jwt/private.key",
		},
	}
}

type PixConfig struct {
	PrefixId string `json:"prefix_id" mapstructure:"prefix_id"`
}

func setupDefaultPixConfig() *PixConfig {
	return &PixConfig{
		PrefixId: "dev",
	}
}

type IdentityConfig struct {
	DatabaseName string `json:"database_name" mapstructure:"database_name"`
	Providers    struct {
		CpfCnpj struct {
			Apikey string `json:"api_key" mapstructure:"api_key"`
		}
		Cnpjws struct {
			ApiKey string `json:"api_key" mapstructure:"api_key"`
		}
	} `json:"providers" mapstructure:"providers"`
}

func setupDefaultIdentityConfig() *IdentityConfig {
	return &IdentityConfig{
		DatabaseName: "identity",
		Providers: struct {
			CpfCnpj struct {
				Apikey string "json:\"api_key\" mapstructure:\"api_key\""
			}
			Cnpjws struct {
				ApiKey string "json:\"api_key\" mapstructure:\"api_key\""
			}
		}{
			CpfCnpj: struct {
				Apikey string "json:\"api_key\" mapstructure:\"api_key\""
			}{
				Apikey: "put-here-your-api-key",
			},
			Cnpjws: struct {
				ApiKey string "json:\"api_key\" mapstructure:\"api_key\""
			}{
				ApiKey: "put-here-your-api-key",
			},
		},
	}
}

type InvoiceManagerConfig struct {
	DatabaseName string `json:"database_name" mapstructure:"database_name"`
	IdentityAddr string `json:"identity_addr" mapstructure:"identity_addr"`
	Providers    struct {
		Enotas struct {
			WebhookUrl    string `json:"webhook_url" mapstructure:"webhook_url"`
			EmissorApiKey string `json:"emissor_api_key" mapstructure:"emissor_api_key"`
		}
	} `json:"providers" mapstructure:"providers"`
}

func setupDefaultInvoiceManagerConfig() *InvoiceManagerConfig {
	return &InvoiceManagerConfig{
		DatabaseName: "invoice_manager",
		IdentityAddr: "http://identity-addr-here",
		Providers: struct {
			Enotas struct {
				WebhookUrl    string "json:\"webhook_url\" mapstructure:\"webhook_url\""
				EmissorApiKey string "json:\"emissor_api_key\" mapstructure:\"emissor_api_key\""
			}
		}{
			Enotas: struct {
				WebhookUrl    string "json:\"webhook_url\" mapstructure:\"webhook_url\""
				EmissorApiKey string "json:\"emissor_api_key\" mapstructure:\"emissor_api_key\""
			}{
				WebhookUrl:    "http://callbackwhurl",
				EmissorApiKey: "put-here-your-api-key",
			},
		},
	}
}

type PaymentCoreConfig struct {
	DatabaseName      string `json:"database_name" mapstructure:"database_name"`
	IdentityAddr      string `json:"identity_addr" mapstructure:"identity_addr"`
	BankAddr          string `json:"bank_addr" mapstructure:"bank_addr"`
	CoinbaseApiKey    string `json:"coinbase_api_key" mapstructure:"coinbase_api_Key"`
	CoinbaseSecretApi string `json:"coinbase_secret_api" mapstructure:"coinbase_secret_api"`
}

func setupDefaultPaymentCoreConfig() *PaymentCoreConfig {
	return &PaymentCoreConfig{
		DatabaseName:      "payments",
		IdentityAddr:      "http://identity-url-here",
		BankAddr:          "http://bank-adapter-url-her",
		CoinbaseApiKey:    "put-here-your-api-key",
		CoinbaseSecretApi: "put-here-your-secret-api",
	}
}

type BbBankAdapterConfig struct {
	DatabaseName      string `json:"database_name" mapstructure:"database_name"`
	BbAppKey          string `json:"bb_app_key" mapstructure:"bb_app_key"`
	BbBasicCredential string `json:"bb_basic_credential" mapstructure:"bb_basic_credential"`
	MtlsPublicKey     string `json:"mtls_public_key" mapstructure:"mtls_public_key"`
	MtlsPrivateKey    string `json:"mtls_private_key" mapstructure:"mtls_private_key"`
}

func setupDefaultBbBankAdapterConfig() *BbBankAdapterConfig {
	return &BbBankAdapterConfig{
		DatabaseName:      "bb_adapter",
		BbAppKey:          "put-here-bb-app-key",
		BbBasicCredential: "put-here-basic-credential",
		MtlsPublicKey:     "path/to/public.key",
		MtlsPrivateKey:    "path/to/private.key",
	}
}

type CashwayBankAdapterConfig struct {
	DatabaseName            string `json:"database_name" mapstructure:"database_name"`
	PixKey                  string `json:"pix_key" mapstructure:"pix_key"`
	PixName                 string `json:"pix_name" mapstructure:"pix_name"`
	StatementPollingEnabled bool   `json:"statement_polling_enabled" mapstructure:"statement_polling_enabled"`
}

func setupDefaultCashwayBankAdapterConfig() *CashwayBankAdapterConfig {
	return &CashwayBankAdapterConfig{
		DatabaseName:            "cashway_adapter",
		PixKey:                  "put-here-pix-key",
		PixName:                 "put-here-pix-name",
		StatementPollingEnabled: true,
	}
}

type BtgBankAdapterConfig struct {
	DatabaseName         string `json:"database_name" mapstructure:"database_name"`
	GoogleCredentialJson string `json:"google_credential_json" mapstructure:"google_credential_json"`
	SheetId              string `json:"sheet_id" mapstructure:"sheet_id"`
}

func setupDefaultBtgBankAdapterConfig() *BtgBankAdapterConfig {
	return &BtgBankAdapterConfig{
		DatabaseName:         "btg_adapter",
		GoogleCredentialJson: "path/to/google/cred.json",
		SheetId:              "put-sheet-id-here",
	}
}

type CreditagBankAdapterConfig struct {
	DatabaseName    string                               `json:"database_name" mapstructure:"database_name"`
	Accounts        map[string]CreditagBankAccountConfig `json:"accounts" mapstructure:"accounts"`
	MtlsPublicKey   string                               `json:"mtls_public_key" mapstructure:"mtls_public_key"`
	MtlsPrivateKey  string                               `json:"mtls_private_key" mapstructure:"mtls_private_key"`
	QrCodeExpiresIn int                                  `json:"qr_code_expires_in" mapstructure:"qr_code_expires_in"`
}

type CreditagBankAccountConfig struct {
	StatementPollingEnabled bool   `json:"statement_polling_enabled" mapstructure:"statement_polling_enabled"`
	PixKey                  string `json:"pix_key" mapstructure:"pix_key"`
	Webhook                 struct {
		Enabled bool   `json:"enabled" mapstructure:"enabled"`
		Url     string `json:"url" mapstructure:"url"`
	} `json:"webhook" mapstructure:"webhook"`
	AccountApi struct {
		ClientId string `json:"client_id" mapstructure:"client_id"`
		Secret   string `json:"secret" mapstructure:"secret"`
	} `json:"account_api" mapstructure:"account_api"`
	QrcodeApi struct {
		ClientId string `json:"client_id" mapstructure:"client_id"`
		Secret   string `json:"secret" mapstructure:"secret"`
	} `json:"qrcode_api" mapstructure:"qrcode_api"`
}

func setupDefaultCreditagBankAdapterConfig() *CreditagBankAdapterConfig {
	return &CreditagBankAdapterConfig{
		DatabaseName: "creditag_adapter",
		Accounts:     make(map[string]CreditagBankAccountConfig),
	}
}

func (s CreditagBankAdapterConfig) GetAccounts() []string {
	accs := []string{}
	for acc := range s.Accounts {
		accs = append(accs, acc)
	}
	return accs
}

func (s CreditagBankAdapterConfig) AccountIsLoad(acc string) bool {
	_, ok := s.Accounts[acc]
	return ok
}

func (s CreditagBankAdapterConfig) GetAccount(acc string) CreditagBankAccountConfig {
	return s.Accounts[acc]
}

func (s CreditagBankAdapterConfig) HasAccounts() bool {
	return len(s.Accounts) > 0
}

type SwapperConfig struct {
	DatabaseName string `json:"database_name" mapstructure:"database_name"`
	PaymentsAddr string `json:"payments_addr" mapstructure:"payments_addr"`
	AuthAddr     string `json:"auth_addr" mapstructure:"auth_addr"`
	NposAdoptUrl string `json:"npos_adopt_url" mapstructure:"npos_adopt_url"`
}

func setupDefaultSwapperConfig() *SwapperConfig {
	return &SwapperConfig{
		DatabaseName: "swapper",
		AuthAddr:     "",
		PaymentsAddr: "",
		NposAdoptUrl: "http://localhost",
	}
}

type AuthConfig struct {
	DatabaseName string      `json:"database_name" mapstructure:"database_name"`
	Schemas      AuthSchemas `mapstructure:"schemas"`
}

type AuthSchemas []AuthSchema

func (s AuthSchemas) IsValidSchema(schemaName string) error {
	if _, err := s.GetSchema(schemaName); err != nil {
		return fmt.Errorf("schema not found")
	}
	return nil
}

func (s AuthSchemas) ToMetaIdentity() map[string]map[string]any {
	meta := map[string]map[string]any{}

	for _, schema := range s {
		meta[schema.Name] = schema.Fields.ToMap()
	}

	return meta
}

func (s AuthSchemas) GetSchema(name string) (AuthSchema, error) {
	for _, schema := range s {
		if name == schema.Name {
			return schema, nil
		}
	}
	return AuthSchema{}, fmt.Errorf("schema not found with name")
}

type AuthSchema struct {
	Name   string           `json:"name" mapstructure:"name"`
	Fields AuthSchemaFields `json:"fields" mapstructure:"fields"`
}

func DecodeSchema(meta map[string]any) (AuthSchema, error) {
	proposeSchema := AuthSchema{
		Fields: make([]AuthSchemaField, 0),
	}
	for k, v := range meta {
		proposeSchema.Name = k

		metaFields, ok := v.(map[string]any)
		if !ok {
			return AuthSchema{}, fmt.Errorf("meta fields is not valid")
		}

		for fk, fv := range metaFields {
			proposeSchema.Fields = append(proposeSchema.Fields, AuthSchemaField{
				Name:  fk,
				Type:  reflect.TypeOf(fv).String(),
				Value: fv,
			})
		}
	}

	return proposeSchema, nil
}

func (s AuthSchema) ToMetaIdentity() map[string]any {
	meta := map[string]any{}
	for _, field := range s.Fields {
		meta[field.Name] = field.ToMetaIdentity()
	}
	return meta
}

type AuthSchemaFields []AuthSchemaField

func (s AuthSchemaFields) ToMap() map[string]any {
	var toMap map[string]any = make(map[string]any)
	for _, field := range s {
		toMap[field.Name] = field.Value
	}
	return toMap
}

func (s AuthSchemaFields) HasField(name string) bool {
	for _, field := range s {
		if field.Name == name {
			return true
		}
	}
	return false
}

func (s AuthSchemaFields) GetField(name string) (AuthSchemaField, error) {
	for _, schema := range s {
		if name == schema.Name {
			return schema, nil
		}
	}

	return AuthSchemaField{}, fmt.Errorf("schema field not found with name")
}

type AuthSchemaField struct {
	Name   string `json:"name" mapstructure:"name"`
	Type   string `json:"type" mapstructure:"type"`
	Unique bool   `json:"unique" mapstructure:"unique"`
	Value  any    `json:"value" mapstructure:"-"`
}

func (s AuthSchemaField) ToMetaIdentity() map[string]any {
	value := s.Value

	switch s.Type {
	case "string":
		return map[string]any{
			s.Name: value.(string),
		}
	case "int":
		return map[string]any{
			s.Name: value.(int),
		}
	case "float64":
		return map[string]any{
			s.Name: value.(float64),
		}
	default:
		log.Panicln("schema field to meta identity type is not supported")
		return nil
	}
}

func setupDefaultAuthConfig() *AuthConfig {
	return &AuthConfig{
		DatabaseName: "auth",
	}
}
