package config

func GetIdentityUriDb() string {
	return stateConfig.GlobalConfig.MongoUriDb(stateConfig.IdentityConfig.DatabaseName)
}

func GetInvoiceManagerUriDb() string {
	return stateConfig.GlobalConfig.MongoUriDb(stateConfig.InvoiceManagerConfig.DatabaseName)
}

func GetPaymentCoreUriDb() string {
	return stateConfig.GlobalConfig.MongoUriDb(stateConfig.PaymentCoreConfig.DatabaseName)
}

func GetBbBankAdapterUriDb() string {
	return stateConfig.GlobalConfig.MongoUriDb(stateConfig.BbBankAdapterConfig.DatabaseName)
}

func GetCashwayBankAdapterUriDb() string {
	return stateConfig.GlobalConfig.MongoUriDb(stateConfig.CashwayBankAdapterConfig.DatabaseName)
}

func GetBtgBankAdapterUriDb() string {
	return stateConfig.GlobalConfig.MongoUriDb(stateConfig.BtgBankAdapterConfig.DatabaseName)
}

func GetCreditagBankAdapterUriDb() string {
	return stateConfig.GlobalConfig.MongoUriDb(stateConfig.CreditagBankAdapterConfig.DatabaseName)
}

func GetSwapperUriDb() string {
	return stateConfig.GlobalConfig.MongoUriDb(stateConfig.SwapperConfig.DatabaseName)
}

func GetAuthUriDb() string {
	return stateConfig.GlobalConfig.MongoUriDb(stateConfig.AuthConfig.DatabaseName)
}

func GetOtcUriDb() string {
	return stateConfig.GlobalConfig.MysqlUriDb("otc")
}
