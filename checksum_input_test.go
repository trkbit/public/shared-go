package trkbitsharedgo

import (
	"testing"

	"gitlab.com/trkbit/public/shared-go/types"
)

func Test_InputChecksum(t *testing.T) {
	r := InputCheckSum(types.CurrencySystemBTGBank, "923434839", "20240110", 235049)
	if !(r.Hash() == "03c7cd5d6b18495a412ee77b230779f9bd421b8c02b4049c01822b81fd4aaba7") {
		t.Error("error: does not match")
		return
	}
}
