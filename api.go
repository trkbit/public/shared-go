package trkbitsharedgo

import (
	"io"
	"net/http"

	"gitlab.com/trkbit/public/shared-go/types"
)

var (
	IDENTITY_API_URL_PRD = ""
)

type api struct {
	client           *http.Client
	identityEndpoint string
}

type Api interface {
	GetCnpj(document string, scrape bool) (*types.Company, *types.Error)
	GetCpf(document string, scrape bool) (*types.Person, *types.Error)
	SetIdentityApi(url string) Api
}

func NewApi(urlIdentityApi string) Api {
	IDENTITY_API_URL_PRD = urlIdentityApi
	return &api{
		client:           &http.Client{},
		identityEndpoint: IDENTITY_API_URL_PRD,
	}
}

func (s *api) SetIdentityApi(url string) Api {
	s.identityEndpoint = url
	return s
}

func (s api) request(method string, uri string, body io.Reader) (types.RawData, error) {
	endpoint := uri

	r := request{client: s.client}
	headers := map[string]string{
		"content-type": "application/json",
	}
	payload, err := r.Do(method, endpoint, headers, body)
	if err != nil {
		return payload, err
	}

	return payload, nil
}
