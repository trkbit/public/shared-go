package trkbitsharedgo

import (
	"log"

	"gitlab.com/trkbit/public/shared-go/types"
)

func (s api) GetCnpj(document string, scrape bool) (*types.Company, *types.Error) {
	uri := s.identityEndpoint + "/identity" + "/" + document
	if scrape {
		uri = uri + "?scrape=true"
	}

	var errResp types.Error
	var response struct {
		Data types.Company `json:"data"`
	}

	payload, err := s.request("GET", uri, nil)

	log.Println(string(payload))
	if err != nil {
		if err := payload.JsonUnmarshal(&errResp); err != nil {
			return nil, &types.Error{400, err.Error()}
		}
		return nil, &errResp
	}

	if err := payload.JsonUnmarshal(&response); err != nil {
		return nil, &types.Error{400, err.Error()}
	}

	return &response.Data, nil
}

func (s api) GetCpf(document string, scrape bool) (*types.Person, *types.Error) {
	uri := s.identityEndpoint + "/identity" + "/" + document
	if scrape {
		uri = uri + "?scrape=true"
	}

	var errResp types.Error
	var response struct {
		Data types.Person `json:"data"`
	}

	payload, err := s.request("GET", uri, nil)
	if err != nil {
		if err := payload.JsonUnmarshal(&errResp); err != nil {
			return nil, &types.Error{400, err.Error()}
		}
		return nil, &errResp
	}

	if err := payload.JsonUnmarshal(&response); err != nil {
		return nil, &types.Error{400, err.Error()}
	}

	return &response.Data, nil
}
